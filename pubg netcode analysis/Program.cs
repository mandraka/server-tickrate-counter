﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;

namespace pubg_netcode_analysis
{

    /*
     Program operuje na eksporcie CSV z programu WireShark, 
     pobiera  wartosc Time z pierwszej kolumny, ktora przybiera wartosc x.xxxxxx     
     konwertuje ja na typ double i przenosi do tablicy
     liczy odstepy czasowe

     */
    class Program
    {
        static void Main(string[] args)
        {
            //Wylicz ile linijek-wartosci kodu bedzie brane pod uwage
            int i = 0;
            string inputPath = Path.GetFullPath("input.csv");
            string outputPath = Path.GetFullPath("results");
            using (var reader = new StreamReader(inputPath))
            {
                while (reader.ReadLine()!=null)
                    i++;
            }
            int dlugoscAnalizy = i;

            //Przygotuj sie do konwersji lancucha znakow z pliku CSV na tablice bezwzglednych wartosci sekundowych i tablice roznic sekundowych
            i=0;
            double[] wartosci= new double [dlugoscAnalizy];
            double[] roznice = new double[dlugoscAnalizy-1];
            double temp;
            string[] s3=new string[6];
            string a;
            //Konwersja
            using (var reader = new StreamReader(inputPath))
            {
                do
                {
                    a = reader.ReadLine();
                    a = a.Replace('"', ' ');
                    s3 = a.Split(',');
                    wartosci[i] = float.Parse(s3[0], CultureInfo.InvariantCulture);
                    i++;
                } while (i<dlugoscAnalizy);
            }

            for (int j=0;j<roznice.Length; j++)
            {
                roznice[j] = wartosci[j + 1] - wartosci[j];
            }

            double liczbaSekund = 0;
            foreach (double r in roznice)
                liczbaSekund += r;
            double[] wartosciSrednie = new double[(int)liczbaSekund];

            //Wpisz na podstawie pelnych sekund wartosci srednie odswiezania serwera
            int k = 0;
            temp = 0;
            int iloscWyslanych = 0;
            foreach (double r in roznice)
            {
                temp += r;
                iloscWyslanych++;

                if (temp>=1)
                {
                    wartosciSrednie[k] = temp / iloscWyslanych;
                    temp = temp-1;
                    iloscWyslanych = 0;
                    k++;
                }
            }

            //Wypisz w konsoli ile sekund pobierano dane i ile probek wykorzystano
            Console.WriteLine("Z ilu sekund liczono: " +wartosciSrednie.Length);
            Console.WriteLine("Ile linijek pobrano: "+ dlugoscAnalizy);

            //Wpisz wynik do pliku- iterator po lewej po prawej wartosc w Hz
            using (StreamWriter writer = new StreamWriter(outputPath))
            {
                int l = 1;
                double tempor;
                foreach (double c in wartosciSrednie)
                {
                    tempor = 1 / c;
                    writer.WriteLine(l + " " + tempor);
                    l++;
                }
            }
        }
    }
}
